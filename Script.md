<!--

SPDX-FileCopyrightText: 2021 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-NC-SA-4.0

-->

# Cocoon Comic Script

## Page 01

Sal: “Charlea, I’m home.”

Charlea: “I’m in here.”

Sal: “What’s wrong, sis?”

Charlea: “I don’t feel too well.”

## Page 02

Sal: “I brought you perfume-wood. It will make you feel better. It smells
so-o-o-o nice.”

Sal: “I put the wood on. You’ll feel better soon.”

Charlea: “I hope so. Thank you, Sal. Please close my door. I want to be alone.”

Sal: “Alright, are you sure you want me to go?”

Charlea: “Yes, Please go away.”

## Page 03

Charlea: “I love you.”

Sal: “I love you too.”

Charlea Thinking: ‘Should I explain it to Sal?’

Charlea Thinking: ‘Sal’s too little to understand.’

Charlea Thinking: ‘Do I even understand?’

## Page 04

## Page 05

Sal Thinking: ‘This is like when Mum and Dad were sick…’

## Page 06

Charlea: ‘I’ll get better.’

Charlea: ‘You’ll see.’

Narrator: ‘Charlea told me they were sick.’

Narrator: The doctors couldn’t heal them.

Sal: ‘Wake up…’

Sal Thinking: ‘Please’

Sal Thinking: ‘Wake up.’

## Page 07

Sal: “Wake up. Wake up!”

## Page 08

Narrator: Since we were born, they’ve called us monsters.

Sal: “Charlea, I’m home.”

Narrator: The whole villiage hates us.

## Page 09

Sal Thinking: ‘Only our parents loved us. They protected us from the mean children’

Crystal: “Little girl, why are you crying?”

## Page 10

Sal: “Where are you? How can you talk in my head?”

Crystal: ‘I’m at your front door, dear, would you please let me in?’

## Page 11

## Page 12

Crystal: “Have you eaten tonight, dear?”

Sal: “No, I didn’t feel like it.”

Crystal: “Well, you sit down and I’ll make something nice for you to eat.”

## Page 13

Crystal: “I’m Crystal Comfort.”

Sal: “Comfort? Are you here to help me?”

Crystal: “Yes Sallina. God created me to help people.”

## Page 14

Sal: “How do you know my name?”

Crystal: “I’ve been watching you for a little while. I travel looking for people
to help.”

Sal: “Where did you come from?”

Crystal: “Another place dear, somewhere you’ve never heard of.”

## Page 15

Sal: “Are you an angel?”

Crystal: “No, dear.”

Sal: “But, you have wings and travel to help people?”

Crystal: “I’m not an angel, but you’re right. I travel to different worlds to
help people.”

Sal: “Ouch! It’s too hot.”

## Page 16

Crystal: “Let me fix that.”

Sal: “This tastes good.”

Crystal: “I’m glad you liked it.”

Sal: “Why did you come, Miss Crystal?”

Crystal: “I came to tell you something very important about your sister.”

## Page 17

Sal: “But, how do you know about my sister?”

Crystal: “I know a lot of things, dear. I came to tell you how to help her.”

Sal: “Why is she in that silk bag?”

Crystal: “Do you know anything about Silkwings when they grow up?”

Sal: “No, Miss Crystal. My parents were humans.”

## Page 18

Crystal: “Well, you see Sal, I am a Silkwing too. Before a Silkwing grows up,
they become sick for a while. It is part of what it means to get older.”

Sal: “How come humans don’t get sick before they grow up?”

Crystal: “I don’t know dear, I just know they are made differently from
Silkwings.”

Sal: “How come you look different from me and Charlea?”

Crystal: “I am a grown up. Grown up Silkwings look very different from when they
were children.”

## Page 19

Sal: “Why are you pretending Charlea will come back? You don’t need to hide it
from me, I know she is dead. I’m not stupid!”

Crystal: “She will come back, dear, if you follow my instructions.”

Sal: “What instructions?”

Crystal: “Your sister has been dead for one day. Silkwings take fourteen days to
come back to life as a grown up. You must help your sister when that happens.”

## Page 20

Sal: “How can I help her?”

Crystal: “Here is a bracelet. When all the stones on the bracelet stop glowing,
it will be time to help your sister.”

Crystal: “When they all stop glowing, you must help your sister out of her cocoon.
She will look very different than when you saw her last.”

## Page 21

Sal: “Will she be pretty like you?”

Crystal: “Yes, dear, very pretty.”

Crystal: “Now dear, you must not try to open the cocoon before all the beads
stop glowing or your sister will never come back. Do you think you can wait that
long?”

Sal: “Yes, Miss Crystal.”

## Page 22

Crystal: “Alright, I need to go now, but I will be praying for you. Remember
that God is the one who will make your sister all grown up. He is remaking her
into a wonderful person inside her cocoon.”

Sal: “Thank you.”

Crystal: “If something really bad happens, put the bracelet on, don’t wear it
unless you really, really need my help.”

## Page 23

Sal: “I love you, Miss Crystal.”

Crystal: “I love you too, Sallina.”

## Page 24

Sal Thinking: ‘She said to put the bracelet on if I needed help. I feel sad, so
I think she wouldn’t mind if I put it on.`

## Page 25

## Page 26

Crystal: “Why did you put the bracelet on when I told you it was only for
emergencies?”

## Page 27

Sal: “I… I… I wanted to talk to you.”

Crystal: “Sallina, I know you feel lonely right now, but you must be patient. I
have very important things to do that I cannot tell you about. People might die
because the bracelet made me leave them in danger when it called me here.”

## Page 28

Sal: “But, I just wanted to talk for a few minutes.”

Crystal: “I know dear, but it hurts for me to travel to your world that fast.
You have plenty of food and firewood. When I feel bad, I read the Holy Writings
and pray to God. He will help you be patient.”

## Page 29

Sal: “Are you angry with me?”

Crystal: “Not anymore. I love you. Please take the bracelet off now.”

## Page 30

## Page 31

Sal Thinking: ‘I should take a peek.’

Sal Thinking: ‘I just need to tug a little.’

## Page 32

Sal Thinking: ‘I guess, I shouldn’t have done that.’

Sal Thinking: ‘I hope nothing bad happens.’

Sal Thinking: ‘Oh no, the cocoon’s bleeding!’

Sal Thinking: ‘Maybe, I can stop it.’

Sal Thinking: ‘Where is the bracelet?’

## Page 33

Sal Thinking: ‘I really need that bracelet.’

Sal Thinking: ‘Where did I leave it?’

Sal Thinking: ‘She might get mad!?’

Sal Thinking: ‘I’ll wait.’

Sal Thinking: ‘No, I need her.’

## Page 34

Crystal: “What is wrong, Sallina?”

Sal: “I tried to look early.”

Sal: “Will she die?”

Crystal: “She is already dead dear. The question is whether she will come back.”

Sal: “Will she come back, Miss Crystal?”

Crystal: “I don’t know. You hurt your sister very badly. I will try to help her.
Pray.”

## Page 35

## Page 36

Sal: “Did you save her?”

Crystal: “I don’t know dear, let’s pray.”

## Page 37

## Page 38

Sal: “A pretty woman came to help me…”

## Page 39

## Page 40

Sal: “…and then I opened your cocoon.”

Sal: “I’m sorry.”

Charlea: “I forgive you, Sal…”

Charlea: “You’re my sister.”

## Page 41

Sal: “But I hurt you.”

Charlea: “I know. I should’ve told you what would happen before I died, but I
didn’t have the energy to.”

Sal: “You have a scar on your face because of me.”

Charlea: “I forgive you; you are my best friend.”

## Page 42

Charlea: “Silkwings choose two best friends to have a special connection with.”

Sal: “Who will you pick?”

Charlea: “You. Would you like that?”

Sal: “Yes!”

## Page 43

Sal: “Who is the other friend?”

Charlea: “The carpenter’s apprentice, Jon. I like him, he’s so kind to me.”

Sal: “Oh, you like him that way. You want him to marry you.”

Charlea: “Do you think he’ll say yes?”

Sal: “Definitely.”

## Page 44

## Page 45

## Page 46

Sal’s sister, Charlea, is sick, very sick. The village doctors can’t help. Does a mysterious woman from another world have the answers to save Charlea?
