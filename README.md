<!--

SPDX-FileCopyrightText: 2021 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-NC-SA-4.0

-->

# Cocoon Comic

[![REUSE status](https://api.reuse.software/badge/codeberg.org/RileyDuffield/CocoonComic)](https://api.reuse.software/info/codeberg.org/RileyDuffield/CocoonComic)

## Naming Convention

Files should be named in this format.

`Cocoon-PageXX-STEP.EXT`

Page numbers should be two digits.

STEP codes are:

0. Raw
1. LineArt
2. Coloring
3. Lettering
4. Final

EXT should be a valid file extension.

## Image Proportions

Width: 1280 px
Height: 1771 px

## Speech Bubble Colors

| Name     | Hex Code                    |
| -------- | --------------------------- |
| Charlea  | #cccccc (20% gray), #80e5ff |
| Crystal  | #8787de, #afafe9            |
| Doctor 0 | #f2f2f2 (5% gray)           |
| Doctor 1 | #ececec (7.5% gray)         |
| Sallina  | #ffffff                     |

## External Assets

| Name                   | Type | URL                                                                            | License   | Attribution                         |
| ---------------------- | ---- | ------------------------------------------------------------------------------ | --------- | ----------------------------------- |
| Chango                 | Font | https://fonts.google.com/specimen/Chango                                       | OFL-1.0   | Fontstage                           |
| Gochi Hand             | Font | https://fonts.google.com/specimen/Gochi+Hand                                   | OFL-1.0   | Huerta Tipográfica                  |
| Louis George Café      | Font | https://www.dafont.com/louis-george-caf.font                                   | custom    | Chen Yining, yiningchen23@gmail.com |
| Potta One              | Font | https://fonts.google.com/specimen/Potta+One                                    | OFL-1.0   | Font Zone 108                       |
| speechbubbles template | SVG  | https://www.peppercarrot.com/0_sources/0ther/vector/speechbubbles-template.svg | CC-BY-4.0 | David Revoy, www.davidrevoy.com     |
