#!/bin/sh

# SPDX-FileCopyrightText: 2022 Riley Duffield <NylaWeothief@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

pandoc --css epub.css -o CocoonComic.epub title.txt \
  CocoonComic.md
