#!/bin/sh

# SPDX-FileCopyrightText: 2023 Riley Duffield <NylaWeothief@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

# Inspired by:
# https://stackoverflow.com/questions/8955425/how-can-i-convert-a-series-of-images-to-a-pdf-from-the-command-line-on-linux

# 1. Find PNGs in the directory.
# 2. Sort the PNGs numerically.
# 3. Turn the PNGs into a PDF.
img2pdf $(find -maxdepth 1 -name '*-Final.png' | sort -V) -o CocoonComic.pdf

# Remove metadata from PDF

mat2 --inplace CocoonComic.pdf
