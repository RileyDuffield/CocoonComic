// SPDX-FileCopyrightText: 2022 DiosYubi, Riley Duffield <NylaWeothief@disroot.org>
// SPDX-License-Identifier: CC-BY-NC-SA-4.0

!!TITLE Cocoon
!!AUTHOR Riley Duffield :link https://codeberg.org/RileyDuffield
@@COVERIMG CocoonComic-Cover-Final.png
!!ILLUSTRATOR DiosYubi :link https://pixelfed.social/DiosYubi
!!DESC Sal’s sister, Charlea, is sick, very sick. The village doctors can’t help. Does a mysterious woman from another world have the answers to save Charlea?
!!COVERART DiosYubi :link https://pixelfed.social/DiosYubi
!!COVERDESC On an illustrated book cover, a girl stands behind her little sister. A mysterious woman looms in the background. Branches encircle the page.
!!SMALL
!!COPYRIGHT 2022 DiosYubi and Riley Duffield CC-BY-NC-SA-4.0
!!RATING AA
!!KYWD cocoon
!!KEYWORDS children, comic, fairy tale, fable, fantasy, fiction, speculative fiction, creative commons
!!LANG EN

// The cover and Page 0 are redundant because the Shanty file includes their information.
// @@IMG :source CocoonComic-Cover-Final.png
// @@IMG :source Cocoon-Page0-Final.png
@@IMG :source Cocoon-Page1-Final.png :alt Page 1: A young girl, named Sallina, picks a plant and runs home and greets her sister, Charlea. Charlea says she doesn't feel too well.
@@IMG :source Cocoon-Page2-Final.png :alt Page 2: A young girl, named Sallina, places sweet-smelling wood in a fire and tells her sister, Charlea, that the scent will help her feel better.
@@IMG :source Cocoon-Page3-Final.png :alt Page 3: A young girl, named Sallina, cries outside her sister's room while her sister weaves a cocoon around herself.
@@IMG :source Cocoon-Page4-Final.png :alt Page 4: A young girl, named Sallina, cries herself to sleep. Then the sun rises, with birds tweeting, and she opens a room, which contains a surprise.
@@IMG :source Cocoon-Page5-Final.png :alt Page 5: A person inside a cocoon leans against the corner of a room. Her little sister, Sallina, weeps and remembers her parents dying.
@@IMG :source Cocoon-Page6-Final.png :alt Page 6: An alien girl, named Charlea, tells her sister, Sallina, that she will get better. Sallina remembers her sick parents dying.
@@IMG :source Cocoon-Page7-Final.png :alt Page 7: An alien girl, named Sallina, shakes a human-sized cocoon, begging for the person inside to wake up.
@@IMG :source Cocoon-Page8-Final.png :alt Page 8: An alien girl, with large antennae, dreams about mean children throwing rocks at her sister, Charlea.
@@IMG :source Cocoon-Page9-Final.png :alt Page 9: A girl, named Sallina, remembers her mother hugging her. Then a cloud of light asks her a why she is crying as she stoops over a human-sized cocoon.
@@IMG :source Cocoon-Page10-Final.png :alt Page 10: A girl, named Sallina, looks over her shoulder with a confused expression. A voice in her head tells her to open a large, round door.
@@IMG :source Cocoon-Page11-Final.png :alt Page 11: A winged woman, with her back to the viewer, removes her hood as she stands before a round door.
@@IMG :source Cocoon-Page12-Final.png :alt Page 12: A winged woman, named Crystal, enters a house through a round door and talks to an alien child, who has wavy hair.
@@IMG :source Cocoon-Page13-Final.png :alt Page 13: A winged woman, named Crystal, makes soup while an alien girl asks questions.
@@IMG :source Cocoon-Page14-Final.png :alt Page 14: An alien girl, named Sallina, asks questions as a winged woman serves homemade soup at a table.
@@IMG :source Cocoon-Page15-Final.png :alt Page 15: An alien girl, named Sallina, asks a winged woman if she is an angel. Sallina reacts because her soup is too hot.
@@IMG :source Cocoon-Page16-Final.png :alt Page 16: A winged woman cools a bowl of soup with a gray cloud, coming out of her hand, and talks to an alien girl.
@@IMG :source Cocoon-Page17-Final.png :alt Page 17: An alien girl asks a winged woman, named Crystal, how she knows her sister. Crystal asks her if she knows how silkwings grow up.
@@IMG :source Cocoon-Page18-Final.png :alt Page 18: A winged woman, named Crystal, tells an alien girl that silkwings look different when they grow up. Crystal shows that she is the same type of alien by transforming her face to look like an insect. 
@@IMG :source Cocoon-Page19-Final.png :alt Page 19: An alien girl, with curly hair, yells at a winged woman, named Crystal. Crystal tells the girl that her sister will come back to life if she waits 14 days to help her sister hatch.
@@IMG :source Cocoon-Page20-Final.png :alt Page 20: An alien girl holds a bracelet, made of 14 beads. A winged woman, named Crystal, tells her that she must help her sister hatch when all the beads stop glowing.
@@IMG :source Cocoon-Page21-Final.png :alt Page 21: An alien girl asks if, when she comes back to life, her sister will be pretty like the woman she is hugging.
@@IMG :source Cocoon-Page22-Final.png :alt Page 22: A winged woman, with large, furry ears, towers over an alien girl. The winged woman wears a simple white dress with a belt at the waist. She tells the girl that she is praying and to put on a bracelet if something bad happens.
@@IMG :source Cocoon-Page23-Final.png :alt Page 23: An alien girl tells a winged woman that she loves her. As she leaves, the woman blows a kiss.
@@IMG :source Cocoon-Page24-Final.png :alt Page 24: An alien girl lays on a couch and debates whether or not to put on a special bracelet, made of round stones.
@@IMG :source Cocoon-Page25-Final.png :alt Page 25: An alien girl covers her eyes as the room fills with light. She looks up in surprise after the light fades.
@@IMG :source Cocoon-Page26-Final.png :alt Page 26: A comic shows a winged woman, with a bat-like face, appearing in a burst of light. She asks, “Why did you put the bracelet on when I told you it was only for emergencies?” Her face gradually transforms to become more human.
@@IMG :source Cocoon-Page27-Final.png :alt Page 27: An alien girl hangs her head. She wears a special bracelet, that summoned a winged woman. The woman tells her to be patient.
@@IMG :source Cocoon-Page28-Final.png :alt Page 28: An alien girl says she wanted to talk to a woman for a few minutes. The woman, named Crystal, says that travelling to the girls world quicly hurts. Crystal tells the girl that she has plenty of supplies, the Holy Writings, and can talk to God.
@@IMG :source Cocoon-Page29-Final.png :alt Page 29: A winged woman, named Crystal, asks an alien girl to remove her bracelet. When the girl does, the woman disappears in a swirl of light.
@@IMG :source Cocoon-Page30-Final.png :alt Page 30: An alien girl clasps her hands together in prayer, while she holds a glowing, bead bracelet. Then, a three-eyed teddy bear holds the bracelet as the beads gradually stop glowing.
@@IMG :source Cocoon-Page31-Final.png :alt Page 31: An alien girl, named Sallina, opens a door and peeks inside a cocoon too early.
@@IMG :source Cocoon-Page32-Final.png :alt Page 32: An alien girl realizes she shouldn't have opened the cocoon. She searches for a bracelet, so it can call her friend to help her.
@@IMG :source Cocoon-Page33-Final.png :alt Page 33: An alien girl scours her house for a bracelet, then hesitates before wearing it. Her friend might get mad for being called again. The girl puts the bracelet on and light streams into the room.
@@IMG :source Cocoon-Page34-Final.png :alt Page 34: An alien girl and a winged woman cry because a human-sized cocoon is damaged. Both girls have insect-like antennae.
@@IMG :source Cocoon-Page35-Final.png :alt Page 35: An alien girl is on her face: praying. Behind her a woman, with bat wings and antennae, uses gray fire to heal a bleeding, human-sized cocoon.
@@IMG :source Cocoon-Page36-Final.png :alt Page 36: A winged woman holds an alien girl until the girl cries herself to sleep.
@@IMG :source Cocoon-Page37-Final.png :alt Page 37: An alien girl awakes as a human-sized cocoon rustles. A blade from inside the cocoon opens it.
@@IMG :source Cocoon-Page38-Final.png :alt Page 38: An alien woman, covered in goo, as thick as molasses, sits up. Her little sister, Sallina, helps her to a bath tub. After the woman is clean and in a new, silk dress, Sallina holds her hands together and smiles in satisfaction.
@@IMG :source Cocoon-Page39-Final.png :alt Page 39: A young woman is wearing a silk, backless dress. Fairy-like wings protrude from her back and straight hair sweeps over her shoulders. She has a button nose, short jaw, and delicate antennae coming out of her forehead.
@@IMG :source Cocoon-Page40-Final.png :alt Page 40: An alien girl talks to her sister, Charlea. Charlea is a beautiful, young woman with fairy-like wings and shoulder-length hair. Charlea forgives her sister for opening the cocoon too early and hugs her.
@@IMG :source Cocoon-Page41-Final.png :alt Page 41: An alien woman puts her arm around her little sister, Sallina. She tells Sallina that she forgives her and that she is her best friend.
@@IMG :source Cocoon-Page42-Final.png :alt Page 42: A young woman, with fairy-like wings and shoulder-length hair, asks her sister, Sallina, if she wants to be one of her two special friends. Sallina agrees, so the woman kisses Sallina's wrist to create her connection as a silkwing.
@@IMG :source Cocoon-Page43-Final.png :alt Page 43: An alien girl asks her sister, Charlea, who the second person she will share a connection with is. Charlea sister daydreams of Jon. The girls agree that Jon will want to marry Charlea.
@@IMG :source Cocoon-Page44-Final.png :alt Page 44: A young woman, holds her little sister’s hand as they walk on a path in the woods. Both girls have insect-like antennae coming out of their heads. They also have long dresses.
// Page 45 is unneeded for the Shanty version.
//@@IMG :source Cocoon-Page45-Final.png

#Credits

Riley wrote the script using Nano and Gedit. The computers used on the project ran free software atop GNU/Linux. To illustrate and letter this comic, we used Krita and Inkscape.

We’re grateful to these artists for creating the fonts and assets we used in this project.

{{https://fonts.google.com/specimen/Chango||Chango}} is licensed under OFL-1.0 by Fontstage.

{{https://fonts.google.com/specimen/Gochi+Hand||Gochi Hand}} is licensed under OFL-1.0 by Huerta Tipográfica.

{{https://www.dafont.com/louis-george-caf.font||Louis George Café}} is licensed under a custom license by Chen Yining, {{mailto:yiningchen23@gmail.com||yiningchen23@gmail.com}}.

{{https://fonts.google.com/specimen/Potta+One||Potta One}} is licensed under OFL-1.0 by HuertaFont Zone 108.

{{https://www.peppercarrot.com/0_sources/0ther/vector/speechbubbles-template.svg||speechbubbles template}} is licensed under CC-BY-4.0 by David Revoy, {{https://www.davidrevoy.com||www.davidrevoy.com}}

// Page 45 is redundant for the Shanty version.
// @@IMG :source Cocoon-Page46-Final.png :alt Page 46: Sal’s sister, Charlea, is sick, very sick. The village doctors can’t help. Does a mysterious woman from another world have the answers to save Charlea?

