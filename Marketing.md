<!--

SPDX-FileCopyrightText: 2021 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-NC-SA-4.0

-->

# Marketing Cocoon

## Copyright

## Cover Art

My first comic, Cocoon, is in the works.

Sal's sister, Charlea, is sick, very sick. The village doctors can't help. Does a mysterious woman from another world have the answers to save Charlea?

Follow me to see new pages as they are released.

Image: CC-BY-NC-ND-4.0

#Art #Comic #Fantasy #Inkscape #Krita #MastoArt

Fonts:
- Chango by Fontstage released under OFL-1.0 (https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
- Louis George Café by Chen Yining (yiningchen23@gmail.com)

| Name              | URL                                          | License   | Attribution                          |
| ----------------- | -------------------------------------------- | --------- | ------------------------------------ |
| Chango            | https://fonts.google.com/specimen/Chango     | OFL-1.0   | Fontstage                            |
| Louis George Café | https://www.dafont.com/louis-george-caf.font | custom    |  Chen Yining, yiningchen23@gmail.com |

@DiosYubi@pixelfed.social and I are working on a new comic book. I wrote the story and lettered it. Yubi made the art in Krita and Inkscape.

### Cover Media Description

Book Cover
Title: Cocoon
A girl stands behind her little sister. A mysterious woman looms in the background. Branches encircle the page.

## Page 01

Cocoon Comic - Page 1

Meet Sallina, the silkwing, as she searches for something to help her sick sister.

Image: CC-BY-NC-SA-4.0

#Art #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 01 Media Description

A young girl picks a plant and runs home and greets her sister, Charlea. Charlea says she doesn't feel too well.

## Page 02

Cocoon Comic - Page 2

Sallina burns perfume-wood to help her sister.

Image: CC-BY-NC-SA-4.0

#Art #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 02 Media Description

A young girl places sweet-smelling wood in a fire and tells her sister the scent will help her feel better.

## Page 03

Cocoon Comic - Page 3

Sallina wants to help her sister, but Charlea is too scared to explain what is going on.

Image: CC-BY-NC-SA-4.0

#Art #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 03 Media Description

A young girl cries outside her sister's room while her sister weaves a cocoon around herself.

## Page 04

Cocoon Comic - Page 4

After a fitful sleep, Sallina awakes to a terrible surprise.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0

### 04 Media Description

In a comic, a young girl cries herself to sleep, then the sun rises, with birds tweeting, and she opens a room, which contains a surprise.

## Page 05

Cocoon Comic - Page 5

Charlea is entirely wrapped in a cocoon, which reminds Sal of when her parents were dying.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 05 Media Description

A person inside a cocoon leans against the corner of a room. Her little sister weeps and remembers her parents dying.

## Page 06

Cocoon Comic - Page 6

Sallina remembers when Charlea first got sick and when her parents were dying.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 06 Media Description

A comic shows an alien girl telling her sister she will get better and other frames show her sick parents dying.

## Page 07

Cocoon Comic - Page 7

Sallina despairs before her sister’s lifeless cocoon.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 07 Media Description

An alien girl shakes a human-sized cocoon, begging for the person inside to wake up.

## Page 08

Cocoon Comic - Page 8

Sallina dreams about how mean the villagers are to silkwings.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 08 Media Description

An alien girl, with large antennae, dreams about mean children throwing rocks at her sister.

## Page 09

Cocoon Comic - Page 9

Sal remembers her parents’ love and a light talks to her.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 09 Media Description

A girl remembers her mother hugging her. Then a cloud of light asks her a why she is crying as she stoops over a human-sized cocoon.

## Page 10

Cocoon Comic - Page 10

A mysterious voice asks Sal to open the door.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 10 Media Description

A girl looks over her shoulder with a confused expression. A voice in her head tells her to open a large, round door.

## Page 11

Cocoon Comic - Page 11

A winged woman visits Sallina.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

### 11 Media Description

A winged woman, with her back to the viewer, removes her hood as she stands before a round door.


## Page 12

Cocoon Comic - Page 12

Crystal Comfort visits Sallina and offers to cook dinner for her.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 12 Media Description

A winged woman enters a house through a round door and talks to an alien child, who has wavy hair.

## Page 13

Cocoon Comic - Page 13

Crystal Comfort cooks dinner for Sallina, who is curious.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 13 Media Description

A winged woman works in a kitchen to make soup while an alien girl asks questions.

## Page 14

Cocoon Comic - Page 14

Crystal serves homemade soup and answers Sallina’s questions.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 14 Media Description

A comic shows an alien girl asking questions as a winged woman serves homemade soup at a table.

## Page 15

Cocoon Comic - Page 15

Sallina asks Crystal if she is an angel.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 15 Media Description

An alien girl asks a winged woman if she is an angel and reacts because her soup is too hot.

## Page 16

Cocoon Comic - Page 16

Crystal tells Sal that she came to deliver an important message.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 16 Media Description

A winged woman cools a bowl of soup with a gray cloud, coming out of her hand, and talks to an alien girl.

## Page 17

Cocoon Comic - Page 17

Crystal and Sal share a meal.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 17 Media Description

A winged woman, named Crystal, and an alien girl, named Sal, with antennae, eat soup at a table while they talk.

Sal: “But, how do you know about my sister?”

Crystal: “I know a lot of things, dear. I came to tell you how to help her.”

Sal: “Why is she in that silk bag?”

Crystal: “Do you know anything about Silkwings when they grow up?”

Sal: “No, Miss Crystal. My parents were humans.”

## Page 18

Cocoon Comic - Page 18

Crystal Shows Sallina that she is a Silkwing too.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 18 Media Description

A winged woman shows an alien girl that she is the same type of alien by transforming her face to look like an insect.

The characters on the comic page said:

Crystal: “Well, you see Sal, I am a Silkwing too. Before a Silkwing grows up,
they become sick for a while. It is part of what it means to get older.”

Sal: “How come humans don’t get sick before they grow up?”

Crystal: “I don’t know dear, I just know they are made differently from
Silkwings.”

Sal: “How come you look different from me and Charlea?”

Crystal: “I am a grown up. Grown up Silkwings look very different from when they
were children.”

## Page 19

Cocoon Comic - Page 19

Sallina doesn’t believe that her sister can come back to life.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 19 Media Description

A comic depicts an alien girl with curly hair yelling at a winged woman named Crystal.

The characters on the page say:

Sal: “Why are you pretending Charlea will come back? You don’t need to hide it from me, I know she is dead. I’m not stupid!”

Crystal: “She will come back, dear, if you follow my instructions.”

Sal: “What instructions?”

Crystal: “Your sister has been dead for one day. Silkwings take fourteen days to come back to life as a grown up. You must help your sister when that happens.”

## Page 20

Cocoon Comic - Page 20

Crystal explains how to help Charlea come back to life.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 20 Media Description

A comic depicts an alien girl (Sal) with antennae holding a bracelet made of 14 beads and talking to a winged woman (Crystal) with wavy hair.

The characters on the page say:

Sal: “How can I help her?”

Crystal: “Here is a bracelet. When all the stones on the bracelet stop glowing,
it will be time to help your sister.”

Crystal: “When they all stop glowing, you must help your sister out of her cocoon.
She will look very different than when you saw her last.”

## Page 21

Cocoon Comic - Page 21

Crystal Comfort gives instructions on waiting for the cocoon to hatch.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 21 Media Description

An alien girl (Sallina) with curly hair runs to a winged woman (Crystal), who holds her face to redirect her gaze.

The characters on the page say:

Sal: “Will she be pretty like you?”

Crystal: “Yes, dear, very pretty.”

Crystal: “Now dear, you must not try to open the cocoon before all the beads
stop glowing or your sister will never come back. Do you think you can wait that
long?”

Sal: “Yes, Miss Crystal.”

## Page 22

Cocoon Comic - Page 22

Crystal Comfort tells Sallina that she can summon her with a bracelet in emergencies.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 22 Media Description

A winged woman (Crystal), with large, furry ears, towers over an alien girl (Sal) who has antennae. The winged woman wears a simple white dress with a belt at the waist.

Crystal: “Alright, I need to go now, but I will be praying for you. Remember
that God is the one who will make your sister all grown up. He is remaking her
into a wonderful person inside her cocoon.”

Sal: “Thank you.”

Crystal: “If something really bad happens, put the bracelet on, don’t wear it
unless you really, really need my help.”

## Page 23

Cocoon Comic - Page 23

Sallina tells Crystal she loves her, so Crystal blows a kiss.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 23 Media Description

In a comic, an alien girl (Sallina) tells a winged woman (Crystal) that she loves her as the woman leaves. Crystal blows a kiss.

Script:

Sallina: “I love you, Miss Crystal.”

Crystal: “I love you too, Sallina.”

## Page 24

Cocoon Comic - Page 24

Sallina feels gloomy, so she is tempted to put on the bracelet.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 24 Media Description

On a comic page, an alien girl (Sallina) lays on a couch and debates whether or not to put on a special bracelet, made of round stones.

Script:

Sal Thinking: ‘She said to put the bracelet on if I needed help. I feel sad, so
I think she wouldn’t mind if I put it on.`

## Page 25

Cocoon Comic - Page 25

Crystal returns after Sallina puts the bracelet on.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 25 Media Description

This comic page has three panels.

In the first panel, an alien girl (Sallina) covers her eyes as she wears a bead bracelet.

The second panel shows the silhouette of a winged woman.

The third panel shows Sallina making an awed face.

## Page 26

Cocoon Comic - Page 26

Crystal returns in a flash of light.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 26 Media Description

A comic shows a winged woman, with a bat-like face, appearing in a burst of light. She asks, “Why did you put the bracelet on when I told you it was only for emergencies?” Her face gradually transforms to become more human.

## Page 27

Cocoon Comic - Page 27

Sallina tells Crystal that she wanted to talk to her.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 27 Media Description

A comic shows a little girl, named Sal, looking up at a winged woman, named Crystal. Crystal is wearing a simple, form-fitting dress and has wavy hair. Her bat-like wings are folded behind her.

Frame 1:

“I… I… I wanted to talk to you,” Sal says as she looks up at Crystal.

Frame 2:

Sal’s head droops.

“Sallina,” Crystal says as she places her hand on Sal’s shoulder. “I know you feel lonely right now, but you must be patient.”

Frame 3:

“I have very important things to do that I cannot tell you about,” Crystal says. “People might die because the bracelet made me leave them in danger when it called me here.”

## Page 28

Cocoon Comic - Page 28

Sal has everything she needs to wait for her sister.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 28 Media Description

A comic shows a little girl, named Sal, and the rooms in her house.

Frame 1:

Sal: “I just wanted to talk for a few minutes.”

Frame 2:

Crystal (out of frame): “I know dear. It hurts for me to travel to your world that fast. You have plenty of food…”

The pantry holds bread, spices, and a bag of potatoes.

Frame 3:

Crystal (out of frame): “And firewood.”

Frame 4:

Crystal (out of frame): “When I feel bad, I read the Holy Writings and pray to God. He will help you be patient.”

A Bible sits on a nightstand and a multi-legged teddy bear peeks out from an unkept, four-poster bed.

## Page 29

Cocoon Comic - Page 29

Sal removes the bracelet and Crystal disappears.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 29 Media Description

A comic shows a little girl, named Sallina, talking to a winged woman, whose back is to the viewer.

Frame 1:

Sallina: “Are you angry with me?”

Crystal: “Not anymore; I love you.”

Frame 2:

Sallina clutches a bracelet made of round, stone beads, which is on her wrist.

Crystal: “Please take the bracelet off now.”

Frame 3:

After Sal removes the bracelet, Crystal disappears in a swirl of light.

Frame 4:

Sallina sits on a stuffed chair with a Bible in her lap and stares into the distance.

## Page 30

Cocoon Comic - Page 30

Sal waits for all the beads to stop glowing. Her bear holds the bracelet for her.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

### 30 Media Description

A comic shows a little girl, named Sallina, praying and waiting for the beads on a necklace to stop glowing.

Frame 1:

Sallina clasps her hands together in prayer, while she holds a glowing, bead bracelet.

Frame 2:

A three-eyed teddy bear holds the bracelet. Nine of the beads are glowing.

Frame 3:

Five of the beads are glowing.

Frame 4:

One bead is glowing.

## Page 31

Cocoon Comic - Page 31

Sal peeks in her sister’s cocoon.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 31 Media Description

A comic shows a little girl, named Sallina, opening a door and looking inside a human-sized cocoon.

Frame 1:

Sallina opens a door and thinks, ‘I should take a peek.’.

Frame 2:

Sallina leans over the cocoon and thinks, ‘I just need to tug a little.’.

Frames 3 and 4:

Sallina pulls the cocoon apart with her fingers.

Frame 5:

Sallina leans over the cocoon.

## Page 32

Cocoon Comic - Page 32

Sal panics because the cocoon is bleeding and she lost the bracelet.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 32 Media Description

A comic shows a little girl, named Sallina, sitting in front of a human-sized cocoon, which is bleeding.

Frame 1:

‘I guess, I shouldn’t have done that,’ Sal thinks while looking at her teddy bear.

Frame 2:

‘I hope nothing bad happens,’ Sal thinks.

Frame 3:

‘Oh no, the cocoon’s bleeding!’ Sal thinks and grows more panicked. ‘Maybe, I can stop it. Where is the bracelet?’

Frame 4:

Sal places her hands on the cocoon to stop the bleeding.

## Page 33

Cocoon Comic - Page 33

Sal finds Crystal’s bracelet and calls her back.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 33 Media Description

A comic shows a little girl, named Sallina, scouring her house to find a bracelet.

Frame 1:

‘I really need that bracelet,’ Sal thought as she rushes through the house.

Frame 2:

‘Where did I leave it?’ Sal wondered as she pauses in front of a couch.

Frame 3:

Sal holds up the bracelet and thinks, ‘She might get mad!?’

Frame 4:

Sal stuffs the bracelet in her pocket and thinks, ‘I’ll wait.’

Frame 5:

‘No, I need her,’ Sal changes her mind.

Frame 6:

Light streams into the room as Sal places the bracelet on.

## Page 34

Cocoon Comic - Page 34

Charlea’s cocoon is damaged badly.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 34 Media Description

A comic shows a little girl, named Sallina, and a woman named Crystal crying because a human-sized cocoon is damaged. Both girls have insect-like antennae.

Frame 1:

“What is wrong, Sallina?” Crystal asks.

“I tried to look early,” Sal sobs as she points at the door to the room the cocoon is in.

Frame 2:

“Will she die?” Sal asks.

Frame 3:

Tears stream down Crystal’s face and she grimaces.

Frame 4:

“She is already dead dear. The question is whether she will come back,” Crystal says as she examines the cocoon.

“Will she come back, Miss Crystal?” Sal asks. Her antennae droop.

“I don’t know. You hurt your sister very badly. I will try to help her. Pray,” Crystal says.

## Page 35

Cocoon Comic - Page 35

Crystal tries to heal Charlea’s cocoon while Sallina prays.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

### 35 Media Description

A comic shows a little girl, named Sallina, on her face praying. Behind her a woman, with bat wings and antennae, uses gray fire to heal a bleeding, human-sized cocoon.

## Page 36

Cocoon Comic - Page 36

Crystal doesn't know if Charlea will live.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 36 Media Description

A comic shows a little girl, named Sallina, and a woman, named Crystal. Both girls have insect-like antennae and wavy hair.

Frame 1:

Crystal hangs her head in sadness and her antennae droop.

Frame 2:

Sal asks, “Did you save her?” as tears stream down her face.

Frame 3:

Crystal says, “I don’t know dear, let’s pray.”

Frame 4:

Crystal puts her hand on Sallina’s head as Sallina leans against her.

Frame 5:

Sallina falls asleep, leaning against Crystal.

## Page 37

Cocoon Comic - Page 37

Charlea’s cocoon starts to move. What’s inside?

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

### 37 Media Description

A comic shows a little girl, named Sallina, with insect-like antennae and wavy hair. There is also a human-sized cocoon.

Frame 1:

The cocoon rustles.

Frame 2:

Sallina perks up to listen.

Frame 3:

Sallina looks at a bead bracelet. None of the beads glow, so it must be time for Charlea to hatch.

Frame 4:

Sallina looks over her shoulder at the cocoon.

Frame 5:

Sallina pinches some of the fabric of the cocoon.

Frame 6:

Salina faces the cocoon.

Frame 7:

A blade comes from inside the cocoon, so Charlea can cut her way out.

Frame 8:

Sallina smiles.

## Page 38

Cocoon Comic - Page 38

Charlea cleans up after hatching.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 38 Media Description

A comic shows a little girl, named Sallina, helping her sister, Charlea, clean up. Both girls have insect-like antennae.

Frame 1:

Charlea’s eyes are closed and she is covered in thick goo, like molasses.

Frame 2:

Sallina helps her sister walk over to a wooden tub, while Sallina says, “A pretty woman came to help me…”.

Frame 3:

Charlea soaks in the tub while Sallina talks to her. There is a leather chair in the foreground and a crackling fire in the fireplace.

Frame 4:

Charlea is clean and in a new, silk dress. She has transparent wings, like fairy wings. Sallina holds her hands together and smiles in satisfaction.

## Page 39

Cocoon Comic - Page 39

Charlea is all cleaned up after hatching.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

### 39 Media Description

A drawing shows a young woman’s profile. She is wearing a silk, backless dress. Fairy-like wings protrude from her back and straight hair sweeps over her shoulders. She has a button nose, short jaw, and delicate antennae coming out of her forehead.

## Page 40

Cocoon Comic - Page 40

Sallina tells Charlea what happened before she hatched.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 40 Media Description

A comic shows a little girl, named Sallina, talking to her sister, Charlea. Charlea is a beautiful, young woman with fairy-like wings and shoulder-length hair. Both girls have insect-like antennae.

Frame 1:

Charlea sips tea as she listens to her little sister.

Sallina says, “…and then I opened your cocoon.”

Frame 2:

Sallina droops her head and says, “I’m sorry.”

Charlea says, “I forgive you, Sal…” as she reaches her hand for Sallina’s shoulder.

Frame 3:

Charlea hugs her Sallina and says, “You’re my sister.”

## Page 41

Cocoon Comic - Page 41

Charlea forgives Sallina for opening the cocoon too early.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 41 Media Description

A comic shows Charlea with her arm around a little girl, named Sallina. Charlea is a beautiful young woman with fairy-like wings and shoulder-length hair. Both girls have insect-like antennae.

“But I hurt you,” Sallina said.

“I know,” Charlea said. “I should’ve told you what would happen before I died, but I didn’t have the energy to.”

“You have a scar on your face because of me,” Sallina’s voice cracked.

“I forgive you; you are my best friend,” Charlea replied.

## Page 42

Cocoon Comic - Page 42

Charlea chooses Sallina to be a host for her symbiont.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #WNVUniverse #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 42 Media Description

A comic shows Charlea, a beautiful, young woman, with fairy-like wings and shoulder-length hair, talking to her little sister, Sallina. Both girls have insect-like antennae.

Frame 1:

“Silkwings choose two best friends to have a special connection with,” Charlea says.

Frame 2:

“Who will you pick?” Sallina asks, looking concerned.

Frame 3:

“You,” Charlea says. “Would you like that?”

Frame 4:

“Yes!” Sallina says, beaming.

Frame 5:

Charlea kisses Sallina’s left hand.

## Page 43

Cocoon Comic - Page 43

Charlea loves Jon, the carpenter’s apprentice.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita #WNVUniverse #MastoArt

Font: Gochi Hand by Huerta Tipográfica released under OFL-1.0
Speech Bubbles by David Revoy, www.davidrevoy.com, released under CC-BY-4.0

### 43 Media Description

A comic shows Charlea, a beautiful young woman with shoulder-length hair, talking to her little sister, Sallina. Both girls have insect-like antennae.

Frame 1:

“Who is the other friend?” Sallina asks her sister.

Frame 2:

“The carpenter’s apprentice, Jon. I like him, he’s so kind to me,” Charlea replies with a faraway look. She imagines a young man, named Jon, who has short hair and a round nose.

Frame 3:

“Oh, you like him that way. You want him to marry you.” Sallina asks.

Frame 4:

Charlea: “Do you think he’ll say yes?” Charlea asks as she hold's Sallina’s hands

“Definitely.” Sallina says.

## Page 44

Cocoon Comic - Page 44

Sallina and Charlea walk to the village to see Jon.

Image: CC-BY-NC-SA-4.0

#Art #ArtWithFreeSoftware #ArtWithOpenSource #Cocoon #Comic #Fantasy #Inkscape #Krita

### 44 Media Description

A comic shows a young woman, holding her little sister’s hand as they walk on a path in the woods. Both girls have insect-like antennae coming out of their heads. They also have long dresses.
